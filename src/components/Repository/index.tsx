import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

import Styles from './styles';

const Repository = ({ data, onRefresh }) => {
  return (
    <Styles.Container>
      <Styles.Name>{data.name}</Styles.Name>
      <Styles.Description>{data.description}</Styles.Description>
      <Styles.Stats>
        <Styles.Stat>
          <Icon name="star" size={16} color="#222222" />
          <Styles.StatCount>{data.stars}</Styles.StatCount>
        </Styles.Stat>
        <Styles.Stat>
          <Icon name="code-fork" size={16} color="#222222" />
          <Styles.StatCount>{data.forks}</Styles.StatCount>
        </Styles.Stat>
      </Styles.Stats>

      <Styles.Refresh onPress={onRefresh}>
        <Styles.RefreshText>
          Atualizar
        </Styles.RefreshText>
        <Icon name="refresh" size={16} color="#3420a0"/>
      </Styles.Refresh>
    </Styles.Container>
  )
}

export default Repository;
